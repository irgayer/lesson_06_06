﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;

namespace AsyncDelegatesLesson
{
    class Program
    {
        private delegate int SumDelegate(int x, int y);
        static void Main(string[] args)
        {
            var currentThread = Thread.CurrentThread;
            Console.WriteLine($"поток {currentThread.ManagedThreadId} работает...");

            //1 шаг - получение проблемы: 
            //var result = Sum(10,15);

            //2 шаг - попытка решения через делегаты из базовых знаний
            //SumDelegate sumDelegate = Sum;
            //var result = sumDelegate.Invoke(10, 15);

            //3 шаг - асинхронный вызов метода у делегатов
            //SumDelegate sumDelegate = Sum;
            //var asyncResult =  sumDelegate.BeginInvoke(10,15, null, null);
            //var result = sumDelegate.EndInvoke(asyncResult);
            //while (!asyncResult.IsCompleted)
            //{
            //    Console.WriteLine("Дождиаемся ответа.");
            //    Thread.Sleep(500);
            //}
            //var result = sumDelegate.EndInvoke(asyncResult);

            //4 шаг - функция обратного вызова возвращается
            SumDelegate sumDelegate = Sum;
            sumDelegate.BeginInvoke(10, 15, SumCallback, "Результат длительной операции: ");

           
            Console.WriteLine($"поток {currentThread.ManagedThreadId} заканчивает работу");
            Console.ReadLine();
        }

        private static void SumCallback(IAsyncResult asyncResult)
        {
            var sumDelegate = (asyncResult as AsyncResult).AsyncDelegate as SumDelegate;
            var result = sumDelegate.EndInvoke(asyncResult);
            Console.WriteLine(asyncResult.AsyncState.ToString() + result);
        }

        /// <summary>
        /// Имитация длительной работы на примере сложения чисел
        /// </summary>
        /// <param name="firstNumber">Первое слагаемое</param>
        /// <param name="secondNumber">Второе слагаемое</param>
        /// <returns>Результат длительной операции сложения</returns>
        static int Sum (int firstNumber, int secondNumber)
        {
            var currentThread = Thread.CurrentThread;
            Console.WriteLine($"поток {currentThread.ManagedThreadId} работает...");

            Thread.Sleep(5000);
            Console.WriteLine($"поток {currentThread.ManagedThreadId} заканчивает работу");
            return firstNumber + secondNumber;
        }
    }
}
